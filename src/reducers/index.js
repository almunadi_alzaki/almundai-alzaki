import {combineReducers} from 'redux';
import users from './user_reducers'
const rootReducer = combineReducers({
    users
})

export default rootReducer;