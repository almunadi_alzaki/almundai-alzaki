import React from 'react';

import {  BrowserRouter,Route } from "react-router-dom";
import ListStudent from './components/ListStudent';
import AddStudent from './components/AddStudent';
import EditStudent from './components/EditStudent';
import AppHeader from './components/Header';
import Home from './pages/Home';

const styles = theme => ({
  body: {
    padding: 3 * theme.spacing.unit,
    [theme.breakpoints.down('xs')]: {
      padding: 2 * theme.spacing.unit,
    },
  },
});


class App extends React.Component {
 
     render() {
      return (
        <BrowserRouter>
          <div>
            <AppHeader />
              <Route path="/add" component={AddStudent}/>
              <Route path="/" exact component={ListStudent}/>
              <Route path="/edit/:id" component={EditStudent}/>
          </div>
        </BrowserRouter>
      )
   }
}

export default App;

