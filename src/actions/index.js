import * as types from '../constants/ActionTypes';

const url = 'http://138.197.158.182:4007/user'

export function userlist(){
let user_data =  fetch(url).then(function(response) {
                return response.json();
                })
                .then(response => response.data)
                .catch(function(err){
                    return err
                })
	return {
        type:types.USERS_LIST,
        payload:user_data
		
    }
}
export function add_user(data){
    data = JSON.stringify(data);
    let user_data =  fetch(url,{
                        method: 'POST', 
                        body: data,
                        headers:{
                            'Content-Type': 'application/json'
                        }
                    }).then(function(response) {
                    return response.json();
                    })
                    .then(response => response.data)
                    .catch(function(err){
                        return err
                    })

    return {
        type:types.ADD_USER,
		editing:false,
        payload:user_data.message
    }
}

export function edit_user(edit_data){
        let user_data =  fetch(url+'/update/'+edit_data._id,{
                        method: 'PUT', 
                        body: JSON.stringify(edit_data),
                        headers:{
                            'Content-Type': 'application/json'
                        }
                        }).then(function(response) {
                        return response.json();
                        })
                        .then(response => response.data)
                        .catch(function(err){
                            return err
                        })
    return {
        type:types.EDIT_USER,
        payload:user_data.message,
		editing:true,
    }
}

export function delete_user(del_data){
    console.log(del_data);
let user_data =  fetch(url+'/'+del_data,{
                    method: 'DELETE', 
                    headers:{
                        'Content-Type': 'application/json'
                    }
                    }).then(function(response) {
                    return response.json();
                    })
                    .then(response => response.data)
                    .catch(function(err){
                        return err
                    })
    return {
        type:types.DELETE_USER,
        payload:user_data
    }
}


