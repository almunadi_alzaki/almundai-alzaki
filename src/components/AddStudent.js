import React,{Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types'
import { add_user} from '../actions';
import {bindActionCreators}from 'redux';

class AddStudent extends React.Component {

	static propTypes = {
		text: PropTypes.string,
		placeholder: PropTypes.string,
		mob_no:PropTypes.string,
		stud_name:PropTypes.string,
		id:PropTypes.number,
		editing: PropTypes.bool,
		newUser: PropTypes.bool
	}


  constructor(props){
  super(props);
  this.state={
    stud_name:'',
    mob_no:'',
    id:'',
    user_data:[],
	editing:false,
	showPopup: false
  }
  
   this.add = this.add.bind(this);
}


 add(){	
    var data = {
      "stud_name":this.state.stud_name,
      "mob_no":this.state.mob_no,
      "id":this.state.id,
  	 "editing":false
    }
    this.state.user_data.push(data);
    
    //dispatch the action ADD_USER
    this.props.add_user(this.state.user_data);
    this.setState({
      stud_name:'',
      mob_no:'',
      id:'',
  	editing:false
    })
  }

   render() {
      return (
         <div>
            <div className="container padding">
              <h3 className="text-muted">Add Student</h3>
              <form name="addStudentForm" className="form-inline">
                <div className="form-group padding">
                  <input type="text" className="form-control" placeholder="Name" value={this.state.stud_name}  onChange={(e) => this.setState({stud_name:e.target.value})} name="stud_name" required/>
                </div>
                <div className="form-group padding">
                  <input type="number" className="form-control" placeholder="Mobile No." placeholder="Mobile Number" value={this.state.mob_no}   onChange={(e) => this.setState({mob_no:e.target.value})} name="mob_no" required/>
                </div>
                {/* <div className="form-group padding">
                  <input type="number" className="form-control" placeholder="Id" placeholder="User id" value={this.state.id}   onChange={(e) => this.setState({id:e.target.value})} name="id" required/>
                </div> */}
                <button type="button" className="btn btn-primary" onClick={ () => this.add() }>Add</button>
              </form>
            </div>
         </div>
      )
   }
}

const mapStateToProps = (state) => {
  return {
    data:state.users,
    
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
          add_user
        },dispatch)
  
}

export default connect(mapStateToProps,mapDispatchToProps)(AddStudent);