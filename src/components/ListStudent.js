import React from 'react';
import {connect} from 'react-redux';
import { userlist,edit_user,delete_user } from '../actions';
import {  Link } from "react-router-dom";
import {bindActionCreators}from 'redux';
class ListStudent extends React.Component {

  componentDidMount () {
		this.props.userlist();
  }
	 
  deleteUser(id){	
		console.log(id);
		this.props.delete_user(id);
		this.props.userlist();
	}
   render() {
      return (
				<div>
					<table className="table">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">Student Name</th>
								<th scope="col">Mobile No.</th>
								<th scope="col">Edit</th>
								<th scope="col">Delete</th>
							</tr>
						</thead>
						<tbody>
							{this.props.data.users?this.props.data.users.map((user)=> (
							<tr>
								<th scope="col">{user._id}</th>
								<td scope="col">{user.stud_name}</td>
								<td scope="col">{user.mob_no}</td>
								<td scope="col">
									<Link to={"/edit/"+user._id}>
										Edit
									</Link>
								</td>
								<td scope="col">  <button type="button" className="btn btn-primary" onClick={()=>this.deleteUser(user._id) }>Delete</button></td>
							</tr>
							)):null}
						</tbody>
					</table>
				</div>
		
      )
   }
}

const mapStateToProps = (state) => {
	return {
	  data:state.users
	}
}
const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		userlist,edit_user,delete_user
	},dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(ListStudent);