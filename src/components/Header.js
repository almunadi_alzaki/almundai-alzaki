import React from 'react';
import {
  AppBar,
  Toolbar,
  Typography,
} from '@material-ui/core';
import {
  Link
} from "react-router-dom";
import '../css/header.css';



class AppHeader extends React.Component {
   render() {
      return (
         <AppBar position="static">
          <Toolbar>
            <Typography variant="title" color="inherit">
              Student App
            </Typography>
            <ul class="nav navbar-nav">
              <li><Link to="/add">Add</Link></li>
              <li><Link to="/">List</Link></li>
            </ul>
          </Toolbar>
        </AppBar>
      )
   }
}

export default AppHeader;