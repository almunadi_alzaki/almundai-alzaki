import React from 'react';
import {connect} from 'react-redux';
import { userlist,add_user,edit_user} from '../actions';
import {bindActionCreators}from 'redux';

class EditStudent extends React.Component {
constructor(){
  super()
  this.state={
    _id:'',
    stud_name:'',
    mob_no:'',
    user_stud:{}
  }
 
}
async componentDidMount(){
 var _id = this.props.match.params.id
 var test = await this.props.data.users?this.props.data.users.map((user)=> {
    if(_id === user._id){
      console.log(user._id);
      console.log(_id);
      this.setState({
        user_stud:user
      })
    }
  }):null
  if(this.state.user_stud){
    console.log(this.state.user_stud);
    this.setState({
      _id:this.state.user_stud._id,
      stud_name:this.state.user_stud.stud_name,
      mob_no:this.state.user_stud.mob_no,
      editing:true
    })
  }
}
edit(){
   var data = {"_id":this.state._id,"stud_name":this.state.stud_name,"mob_no":this.state.mob_no}
    this.props.edit_user(data);
    this.setState({
      stud_name:'',
      mob_no:'',
      _id:'',
    
    })
    console.log(this.props.data.edit_user);
   
 }
  render() {
    return (
      <div>
        <div className="container padding">
          <h3 className="text-muted">Edit Student Info</h3>
          <form name="addStudentForm" className="form-inline">
            <div className="form-group padding">
              <input type="text" className="form-control" placeholder="Name" value={this.state.stud_name}  onChange={(e) => this.setState({stud_name:e.target.value})} name="stud_name" required/>
            </div>
            <div className="form-group padding">
              <input type="number" className="form-control" placeholder="Mobile No." placeholder="Mobile Number" value={this.state.mob_no}   onChange={(e) => this.setState({mob_no:e.target.value})} name="mob_no" required/>
            </div>
            <div className="form-group padding">
              <input type="text" className="form-control" placeholder="Id" placeholder="User id" value={this.state._id}   onChange={(e) => this.setState({_id:e.target.value})} name="_id" required disabled/>
            </div>
            <button type="button" className="btn btn-primary" onClick={ () => this.edit() }>Edit</button>
          </form>
        </div>
     </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    data:state.users,
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    userlist,
    add_user,edit_user
  },dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(EditStudent);
